import FileData, sys, os, json, InputClasses, DataTypeDefinitionClasses
from PySide6.QtWidgets import QApplication, QDialog, QLineEdit, QPushButton, QVBoxLayout, QLabel, QPlainTextEdit, QMainWindow, QVBoxLayout, QBoxLayout, QFrame, QWidget, QGroupBox, QScrollArea, QLayout, QHBoxLayout, QGridLayout, QTreeWidget, QSizePolicy, QCheckBox, QAbstractButton, QMenu, QComboBox, QPlainTextEdit, QMessageBox, QFileDialog
from PySide6.QtCore import QEnum, QFlag, QObject, Qt, QMargins
from PySide6.QtGui import QIcon, QAction

MetaDataPath = "metadata/"
Profiles = os.listdir(MetaDataPath)

WorkingFile = FileData.FileData()
IsEditing = False

#a function to change the editing mode
def SetEditing(NewValue):
    global IsEditing
    if NewValue:
        IsEditing = True
        WindowContent.Main.EditButton.hide()

    else:
        IsEditing = False
        WindowContent.Main.EditButton.show()

#a procedure that edits the working profile
def EditCurrentProfile():
    SetEditing(True)
    
    #clears the layout
    i = ProfileWidget.Layout.takeAt(0)
    while i:
        i.widget().deleteLater()
        i = ProfileWidget.Layout.takeAt(0)

    Iterations = 0
    for Key in WorkingFile.Configs:
        Config = WorkingFile.Configs[Key]
        
        Property = PropertyEditingWidget(Key,Config)

        ProfileWidget.Layout.addWidget(Property)
        Iterations += 1
    
    AddPropertyButton = QPushButton("+")
    AddPropertyButton.clicked.connect(AppendPropertyEditingWidget)
    ProfileWidget.Layout.addWidget(AddPropertyButton)

def AppendPropertyEditingWidget():
    ProfileWidget.Layout = WindowContent.Main.MainElement.Layout

    DefaultMetadata = {"Value" : "True",
                       "StartLine" : "",
                       "EndLine" : "",
                       "DataType" : {
                           "Type" : "Bool",
                           "Capitalize" : False
                           },
                       "Note" : ""
                       }

    ProfileWidget.Layout.insertWidget(ProfileWidget.Layout.count()-1, PropertyEditingWidget("", DefaultMetadata))


#a function that changes the current working file
#Path: path to the file containing profile metadata
def ChangeWorkingFile(Path):

    ProfileWidget.Layout = WindowContent.Main.MainElement.Layout
    
    #clears the layout
    i = ProfileWidget.Layout.takeAt(0)
    while i:
        i.widget().deleteLater()
        i = ProfileWidget.Layout.takeAt(0)

    #getting the config file metadata
    ConfigFile = open(Path, "r")
    ConfigFileContent = ConfigFile.read()
    ConfigFile.close()
    #print(ConfigFileContent)
    #importing metadata into the current working file
    WorkingFile.ImportFromJson(ConfigFileContent)


    Iterations = 0
    for Key in WorkingFile.Configs:
        Config = WorkingFile.Configs[Key]
        
        Property = PropertyWidget(Key,Config)

        ProfileWidget.Layout.addWidget(Property)
        Iterations += 1

#a procedure that applies the changes made and updates the config file
def ApplyChanges():
    global IsEditing

    if IsEditing:
        WorkingFile.Configs.clear()
        for i in range(ProfileWidget.Layout.count() - 1):
            ConfigResult = ProfileWidget.Layout.itemAt(i).widget().GetDataTypeMetadata()
            #print(WorkingFile.Configs)
            if ConfigResult: #if the ConfigResult is valid
                WorkingFile.Configs[ConfigResult["Key"]] = ConfigResult["Value"]

        WorkingFile.ExportToJson()
    else:
        #looping through every input widget and getting their value, then overwriting the WorkingFile data with the new values and applying the changes
        for i in range(ProfileWidget.Layout.count()):
            ConfigResult = ProfileWidget.Layout.itemAt(i).widget().GetResults()
            WorkingFile.Configs[ConfigResult["Key"]]["Value"] = ConfigResult["Value"]

        WorkingFile.ApplyConfigs()


#a procedure that cancels all changes and returns to the initial values
def ReturnToInitialValues():
    global IsEditing
    if not IsEditing:
        for i in range(ProfileWidget.Layout.count()):
            ProfileWidget.Layout.itemAt(i).widget().ReturnToInitialValue()



class ToolTipEditiorDialog(QDialog):
    def __init__(self, ToolTip):
        super(ToolTipEditiorDialog, self).__init__()

        self.Layout = QVBoxLayout()
        self.setLayout(self.Layout)
        
        self.TextEditWidget = QPlainTextEdit()
        self.TextEditWidget.setPlainText(ToolTip)

        self.ConfirmButton = QPushButton("Confirm")
        self.ConfirmButton.clicked.connect(self.ConfirmChanges)

        self.Layout.addWidget(self.TextEditWidget)
        self.Layout.addWidget(self.ConfirmButton)
    
    def ConfirmChanges(self):
        self.accept()

    def GetResults(self):
        return self.TextEditWidget.toPlainText()

class SelectionDialog(QDialog):
    def __init__(self, FileContent, CommentChar):
        super(SelectionDialog, self).__init__()

        self.Layout = QVBoxLayout()
        self.setLayout(self.Layout)
        self.resize(1000,1000)
        
        self.TextEditWidget = QPlainTextEdit()
        self.TextEditWidget.setPlainText(FileContent)
        self.TextEditWidget.setReadOnly(True)
        self.TextEditWidget.setLineWrapMode(QPlainTextEdit.NoWrap)
        self.TextEditWidget.selectionChanged.connect(self.GetSelection)

        self.Cursor = self.TextEditWidget.textCursor()

        self.Selection = {"Start" : 0, "End" : 0}
        self.Results = {"StartLine" : None, "EndLine" : None}
        self.FileContent = FileContent
        self.CommentChar = CommentChar

        self.ConfirmButton = QPushButton("Confirm")
        self.ConfirmButton.clicked.connect(self.ConfirmChanges)

        self.Layout.addWidget(self.TextEditWidget)
        self.Layout.addWidget(self.ConfirmButton)
    
    def ConfirmChanges(self):
        #getting the substring between where the line starts and where the selection starts
        CharIndex = self.Selection["Start"]
        Char = self.FileContent[CharIndex]
        while Char != "\n":
            CharIndex -= 1
            Char = self.FileContent[CharIndex]
        StartLine = self.FileContent[CharIndex + 1 : self.Selection["Start"]]

        #getting the substring between where the selection ends and where the line ends or comment begins
        CharIndex = self.Selection["End"]
        Char = self.FileContent[CharIndex]
        while Char != "\n" and Char != self.CommentChar:
            CharIndex += 1
            Char = self.FileContent[CharIndex]
        EndLine = self.FileContent[self.Selection["End"] : CharIndex].rstrip()

        self.Results = {"StartLine" : StartLine, "EndLine" : EndLine}
        self.accept()

    def GetSelection(self):
        self.Cursor = self.TextEditWidget.textCursor()
        self.Selection = {"Start" : self.Cursor.selectionStart(), "End" : self.Cursor.selectionEnd()} 

    def GetResults(self):
        return self.Results

class PropertyEditingWidget(QWidget):
    def __init__(self, Key, ConfigData):
        super(PropertyEditingWidget, self).__init__()

        self.Layout = QHBoxLayout()
        self.setLayout(self.Layout)
        self.Key = Key
        self.MetaData = ConfigData

        self.PropertyLabel = QLineEdit(Key)
        self.PropertyLabel.setFixedWidth(300)

        self.ToolTipButton = QPushButton("change ToolTip")
        self.ToolTipButton.clicked.connect(self.ChangeToolTip)
        self.ToolTipButton.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)


        self.PropertySelectionButton = QPushButton("select the property")
        self.PropertySelectionButton.clicked.connect(self.SelectProperty)
        self.PropertySelectionButton.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        self.DataTypeSelectionBox = QComboBox()
        self.DataTypeSelectionBox.addItems(InputClasses.DataTypesInputs.keys())
        self.DataTypeSelectionBox.setFixedWidth(75)
        self.DataTypeSelectionBox.setCurrentText(self.MetaData["DataType"]["Type"])
        self.DataTypeSelectionBox.currentTextChanged.connect(self.ChangeDataType)

        #for anyone confused about the line below.
        #in the "DataTypeDefinitionClasses" module i have a dictionary named "DataTypeWidgets" which has the datatypes as keys and custom widget classes as the values
        #I pass the DataType metadata as an argument to the constructor
        self.DataTypeMetadataWidget = DataTypeDefinitionClasses.DataTypeWidgets[ConfigData["DataType"]["Type"]](ConfigData["DataType"])

        self.RaiseButton = QPushButton("up")
        self.RaiseButton.clicked.connect(self.Raise)
        self.RaiseButton.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        self.LowerButton = QPushButton("down")
        self.LowerButton.clicked.connect(self.Lower)
        self.LowerButton.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        self.DeleteButton = QPushButton("x")
        self.DeleteButton.setFlat(True)
        self.DeleteButton.setFixedWidth(20)
        self.DeleteButton.clicked.connect(self.Delete)

        self.Layout.addWidget(self.PropertyLabel)
        self.Layout.addWidget(self.ToolTipButton)
        self.Layout.addWidget(self.PropertySelectionButton)
        self.Layout.addWidget(self.DataTypeSelectionBox)
        self.Layout.addWidget(self.DataTypeMetadataWidget)
        self.Layout.addWidget(self.RaiseButton)
        self.Layout.addWidget(self.LowerButton)
        self.Layout.addWidget(self.DeleteButton)
    
    def GetDataTypeMetadata(self):
        self.MetaData["DataType"] = self.DataTypeMetadataWidget.GetDataTypeMetadata()
        if self.PropertyLabel.text() and self.MetaData["StartLine"] and self.MetaData["EndLine"]:
            return {"Key" : self.PropertyLabel.text(), "Value" : self.MetaData}
        else:
            return None

    def ChangeToolTip(self):
        self.ToolTipDialog = ToolTipEditiorDialog(self.MetaData["Note"])
        if self.ToolTipDialog.exec():
            self.MetaData["Note"] = self.ToolTipDialog.GetResults()
    
    def SelectProperty(self):
        self.SelectionDialog = SelectionDialog(WorkingFile.FileContent, WorkingFile.Comment)
        if self.SelectionDialog.exec():
            Results = self.SelectionDialog.GetResults()
            self.MetaData["StartLine"] = Results["StartLine"]
            self.MetaData["EndLine"] = Results["EndLine"]

    def ChangeDataType(self, NewType):
        DefaultDataTypeMetadata = FileData.DataTypes[NewType] #the default datatype metadata values
        self.DataTypeMetadataWidget.deleteLater()
        self.DataTypeMetadataWidget = DataTypeDefinitionClasses.DataTypeWidgets[NewType](DefaultDataTypeMetadata)
        self.Layout.addWidget(self.DataTypeMetadataWidget) 

    def Delete(self):
        self.deleteLater()

    def Raise(self):
        ProfileWidget.RaiseElement(self)

    def Lower(self):
        ProfileWidget.LowerElement(self)

class PropertyWidget(QWidget):
    def __init__(self, Key, ConfigData):
        super(PropertyWidget, self).__init__()

        self.Layout = QHBoxLayout()
        self.setLayout(self.Layout)

        self.Key = Key
        self.HasUnsavedChanges = False

        #creating the label, its a QWidget instead of a label in case i want to add a icon at the end signalling there is a tool tip to the user
        self.PropertyLabel = QWidget()
        #self.PropertyLabel.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        self.PropertyLabelLayout = QHBoxLayout(self.PropertyLabel)
        self.PropertyLabelLayout.addWidget(QLabel(Key))

        #adding the tooltip
        if ConfigData["Note"]:
            #TO DO: add a icon with a questionmark so that the user knows there is a tooltip
            self.PropertyLabel.setToolTip(ConfigData["Note"])

        #creating the input widget
        self.InputWidget = InputClasses.DataTypesInputs[ConfigData["DataType"]["Type"]](ConfigData["Value"], ConfigData["DataType"])

        #creating the "return to defaults" widget
        #TO DO: make the return to default have an icon instead of this crap
        self.ReturnButton = QPushButton("Return to Defaults")
        self.ReturnButton.setFixedSize(125,25)
        self.ReturnButton.setEnabled(False)
        self.ReturnButton.clicked.connect(self.ReturnToInitialValue)

        self.Layout.addWidget(self.PropertyLabel)
        self.Layout.addWidget(self.InputWidget)
        self.Layout.addWidget(self.ReturnButton)

        try:
            self.InputWidget.Changed.connect(self.ValueChanged)
        except Exception as e:
            print("unable to connect the event")

    def GetResults(self):
        self.Result = self.InputWidget.GetResults()
        return {"Key" : self.Key, "Value" : (self.Result)}

    def ReturnToInitialValue(self):
        self.InputWidget.ReturnToInitialValue()
        self.ReturnButton.setEnabled(False)

    def ValueChanged(self, HasUnsavedChanges):
        if HasUnsavedChanges:
            self.ReturnButton.setEnabled(True)
        else:
            self.ReturnButton.setEnabled(False)
        self.HasUnsavedChanges = HasUnsavedChanges

#side panel
class SidePanelWidget(QWidget):
    def __init__(self):
        super(SidePanelWidget, self).__init__()

        #creates the main layout containing the LabelWithButton and the ScrollableArea that has the config files
        self.VLayout = QVBoxLayout(self) 
        QLayout.setAlignment(self.VLayout, Qt.Alignment.AlignTop) #sets the alignment of the elements in the VLayout to stack from top
        self.setFixedWidth(175)

        #creates the LabelWithButton that has the label saying "configs" and has the button which adds config files to the ScrollableArea list
        self.LabelWithButton = QWidget()
        self.HLayout = QHBoxLayout(self.LabelWithButton) #the layout which contains the button and the label
        self.HLayout.addWidget(QLabel("Profiles")) #the label within HLayout

        #the button for creating new config profiles and importing them
        self.NewButton = QPushButton("new")
        self.NewButton.setFlat(True)

        self.NewButtonMenu = QMenu()
        self.MenuElement = QAction("New profile")
        self.MenuElement.triggered.connect(self.CreateProfile)

        self.ImportMenuElement = QAction("Import profile")
        self.ImportMenuElement.triggered.connect(self.ImportProfile)

        self.NewButtonMenu.addAction(self.MenuElement)
        self.NewButtonMenu.addAction(self.ImportMenuElement)



        self.NewButton.setMenu(self.NewButtonMenu)

        self.HLayout.addWidget(self.NewButton)

        #creates the ScrollableArea that has the profiles listed and allows the user to chose one
        self.ScrollableArea = QScrollArea()
        self.ScrollableArea.setWidgetResizable(True)

        self.ScrollableAreaContent = QWidget() #the content of the ScrollableArea

        self.ProfileLayout = QVBoxLayout(self.ScrollableAreaContent)
        self.ProfileLayout.setContentsMargins(QMargins(0,0,0,0))
        QLayout.setAlignment(self.ProfileLayout, Qt.Alignment.AlignTop) #sets the alignment of the elements in the VLayout to stack from top

        #adding the config buttons to the content of ScrollableArea
        for i in Profiles:
            i= i.split(".")[0] #getting rid of the file extension
            ProfileButton = QPushButton(i)
            ProfileButton.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Fixed)
            ProfileButton.setFlat(True)
            ProfileButton.clicked.connect(
                    lambda _="", Name=i: self.ProfileChosen(Name) #calls extra data to the function, the extra data being the name of the file
                    )
            self.ProfileLayout.addWidget(ProfileButton)

        self.ScrollableArea.setWidget(self.ScrollableAreaContent) #adding content to the ScrollableArea

        #adds the ScrollableArea and LabelWithButton to the main layout
        self.VLayout.addWidget(self.LabelWithButton)
        self.VLayout.addWidget(self.ScrollableArea)

    #a function thats called when a new config file is chosen
    #Name: the name of the file
    def ProfileChosen(self, Name):
        global MetaDataPath
        Name = MetaDataPath + "/" + Name +".json" #adding the correct file extension
        ChangeWorkingFile(Name) 

    def CreateProfile(self):
        print("creating a new config!") 

    def ImportProfile(self):
        print("importing!!")
        FileName = QFileDialog.getOpenFileName(self, "Chose file", "/home/{}/".format(os.getlogin()))[0]
        try:
            File = open(FileName, "r")
            FileContent = json.loads(File.read())
            File.close()
            os.system("cp {} {}".format(FileName, FileContent["MetaDataPath"]))

            FileName = FileName.split("/")[-1].split(".")[0] #getting rid of the file extension, I HATE THIS LINE
            ProfileButton = QPushButton(FileName)
            ProfileButton.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Preferred)
            ProfileButton.setFlat(True)
            ProfileButton.clicked.connect(
                    lambda _="", Name=FileName: self.ProfileChosen(Name) #calls extra data to the function, the extra data being the name of the file
                    )
            self.ProfileLayout.addWidget(ProfileButton)

        except Exception:
            print(Exception)
        


class Main(QWidget):
    def __init__(self):
        super(Main, self).__init__()
        
        self.VLayout = QVBoxLayout(self)
        QLayout.setAlignment(self.VLayout, Qt.Alignment.AlignTop) #sets the alignment of the elements in the VLayout to stack from top

        #creates the element containing the apply button, the edit button
        self.LackOfABetterName = QWidget()
        self.HLayout = QHBoxLayout(self.LackOfABetterName) #the layout which contains the elements
        QLayout.setAlignment(self.HLayout, Qt.Alignment.AlignRight) #sets the alignment of the elements in the layout to stack from right

        #the edit button
        self.EditButton = QPushButton("Edit") 
        self.HLayout.addWidget(self.EditButton)
        self.EditButton.clicked.connect(self.EditProfile)

        #the cancel button
        self.CancelButton = QPushButton("Cancel Changes")
        self.HLayout.addWidget(self.CancelButton)
        self.CancelButton.clicked.connect(self.CancelChanges)

        #the apply button
        self.ApplyButton = QPushButton("Apply") 
        self.ApplyButton.setStyleSheet("background-color: lightgreen")
        self.HLayout.addWidget(self.ApplyButton)
        self.ApplyButton.clicked.connect(self.ApplyButtonClicked)


        #is the main space for editing configs and config profiles
        self.MainScrollableArea = QScrollArea()
        self.MainScrollableArea.setWidgetResizable(True)
        #self.MainScrollableArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.MainScrollableArea.setMinimumWidth(900)
        #self.MainElement = QWidget() #the content of the main scrollablearea

        self.MainElement = ProfileWidgetClass()
        global ProfileWidget
        ProfileWidget = self.MainElement

        self.MainScrollableArea.setWidget(self.MainElement)

        self.VLayout.addWidget(self.LackOfABetterName)
        self.VLayout.addWidget(self.MainScrollableArea)

    def ApplyButtonClicked(self):
        global IsEditing
        PromptOutput = QMessageBox.question(self, "Confirm", "Apply changes?",
                                            buttons=QMessageBox.Discard | QMessageBox.Apply,
                                            defaultButton=QMessageBox.Apply)

        if PromptOutput == QMessageBox.Apply:
            if IsEditing:
                ApplyChanges()
                ChangeWorkingFile(WorkingFile.MetaDataPath)
                SetEditing(False)
            else:
                ApplyChanges()

    def CancelChanges(self):
        global IsEditing
        PromptOutput = QMessageBox.question(self, "Confirm", "cancel changes?",
                                            buttons=QMessageBox.Discard | QMessageBox.Apply,
                                            defaultButton=QMessageBox.Apply)
        if PromptOutput == QMessageBox.Apply:
            if IsEditing:
                ChangeWorkingFile(WorkingFile.MetaDataPath)
                SetEditing(False)
            else:
                ReturnToInitialValues()

    def EditProfile(self):

        if ProfileWidget.HasAnyUnsavedChanges():
            PromptOutput = QMessageBox.question(self, "Confirm", "you have unsaved changes, Save Changes?",
                                            buttons=QMessageBox.Discard | QMessageBox.Apply,
                                            defaultButton=QMessageBox.Apply)
            if PromptOutput == QMessageBox.Apply:
                ApplyChanges()
                EditCurrentProfile()
        else:
            EditCurrentProfile()

#a widget containing all property widgets and stuff inside the scroll area
#its a stupid name for a class but i aint got anthing better
class ProfileWidgetClass(QWidget):
    def __init__(self):
        super(ProfileWidgetClass, self).__init__()

        self.Layout = QVBoxLayout()
        self.setLayout(self.Layout)
        QLayout.setAlignment(self.Layout, Qt.Alignment.AlignTop) #sets the alignment of the elements in the VLayout to stack from top

        self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)

    def HasAnyUnsavedChanges(self):
        global IsEditing
        #this function does nothing if is in editing mode
        if not IsEditing:
            for i in range(self.Layout.count()):
                if self.Layout.itemAt(i).widget().HasUnsavedChanges:
                    return True

        return False

    def RaiseElement(self, WidgetToRaise):
        Index = self.Layout.indexOf(WidgetToRaise)
        if Index != 0:
            self.Layout.takeAt(Index)
            self.Layout.insertWidget(Index - 1, WidgetToRaise)

    def LowerElement(self, WidgetToRaise):
        Index = self.Layout.indexOf(WidgetToRaise)
        if Index < self.Layout.count() - 2:
            self.Layout.takeAt(Index)
            self.Layout.insertWidget(Index + 1, WidgetToRaise)

class Window(QWidget):
    def __init__(self, parent=None):
        super(Window, self).__init__()

        self.layout = QBoxLayout(QBoxLayout.LeftToRight, self)
        self.SidePanel = SidePanelWidget()
        self.Main = Main()

        self.layout.addWidget(self.SidePanel)
        self.layout.addWidget(self.Main)




app = QApplication(sys.argv)


MainWindow = QMainWindow()
WindowContent = Window()
MainWindow.setCentralWidget(WindowContent)
MainWindow.show()
sys.exit(app.exec())
