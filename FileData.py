import json, re, os

DataTypes = {
        "Bool" : {"Type" : "Bool", "Capitalize" : False},
        "Int" : {"Type" : "Int", "MaxValue" : None, "MinValue" : None, "Slider" : False},
        "Float" : {"Type" : "Float", "MaxValue" : None, "MinValue" : None, "Precision" : 2, "Slider" : False },
        "String" : {"Type" : "String", },
        "FilePath" : {"Type" : "FilePath", },
        "List" : {"Type" : "List", "Values" : []},
        "Font" : {"Type" : "Font"},
        "Color" : {"Type" : "Color", "Format" : "HEX"} #for the time being HEX is the only format so i didnt creat a special class to define it with gui
        }

class FileData:

    def __init__(self):
        self.FilePath = "" #the path to the config file
        self.MetaDataPath = "metadata/{}.json".format(self.FilePath.rsplit("/")[-1]) #dirty.
        self.Title = ""
        self.Comment = "#" #the symbol which starts comments in the config file
        self.Configs = {}
        self.ApplyCommand = r"" #the shell run when the settings are applied, used when a program needs to be restarted after configuring
        self.ApplyNote = "" #the note displayed to the user when applying


    #imports data from a json string, the "ImportedContent" is the json
    def ImportFromJson(self, ImportedContent):
        ImportedContent = json.loads(ImportedContent) #turns json into a python dictionary

        self.FilePath = ImportedContent["FilePath"]
        self.MetaDataPath = ImportedContent["MetaDataPath"]
        self.Title = ImportedContent["Title"]
        self.Comment = ImportedContent["Comment"]
        self.Configs = ImportedContent["Configs"]
        self.ApplyCommand = ImportedContent["ApplyCommand"]
        self.ApplyNote = ImportedContent["ApplyNote"]

        tmp = open(self.FilePath, "r")
        self.FileContent = tmp.read()
        tmp.close()

        self.SynchronizeWithConfigFile()

    #Synchronizes the values from the config file values and the FILEDATA values
    def SynchronizeWithConfigFile(self):
        #updating the file content
        try:
            #tmp is just a file to open the config file and save its contnent to another variable, i dont have a better var name
            tmp = open(self.FilePath, "r")
            self.FileContent = tmp.read()
            tmp.close()
        except:
            print("unable to open the config file, failed to synchronize")

        for i in self.Configs:
            CurrentConfig = self.Configs[i]
            Regex = CurrentConfig["StartLine"] + "(?P<Value>.*)" + CurrentConfig["EndLine"] + "(?P<Comment> *" + self.Comment + "[^\n]*)?" 
            Match = re.search(Regex, self.FileContent)
            if Match:
                ValueInFile = Match.group("Value")
                if ValueInFile:
                    self.Configs[i]["Value"] = ValueInFile
                else:
                    print("unable to parse the config file and synchronize the values")
                    break
            else:
                print("didnt find property:", i)

    #ConfigName : the name of the configuration option
    #Value : the current value of the configuration option
    #StartLine : the beginning of the line before the value
    #EndLine : the end of the line after the value
    #DataType: the type of data, used so that the graphical interface knows what widget to use when assigning the data
    #Note : a description of what the configuration option does
    def AddConfig(self, ConfigName, Value, StartLine, EndLine, DataType, Note):
        self.Configs[ConfigName] = {"Value" : Value,
                                    "StartLine" : StartLine,
                                    "EndLine" : EndLine,
                                    "DataType" : DataType,
                                    "Note" : Note}

    #synchronises the external file's content with the configs
    def ApplyConfigs(self):
        #insert code here that spawns a popup gui to the user that displays the note and asks to confirm the apply

        #looks for every config option in FileContent and replaces it with the updated values
        for i in self.Configs:
            CurrentConfig = self.Configs[i]
            Regex = CurrentConfig["StartLine"] + "(?P<Value>.*)" + CurrentConfig["EndLine"] + "(?P<Comment> *" + self.Comment + "[^\n]*)?"
            MatchObject = re.search(Regex, self.FileContent)

            #whether the line has a comment
            if MatchObject.group("Comment"):
                ModifiedLine = CurrentConfig["StartLine"] + CurrentConfig["Value"] + CurrentConfig["EndLine"] + MatchObject.group("Comment")
            else:
                ModifiedLine = CurrentConfig["StartLine"] + CurrentConfig["Value"] + CurrentConfig["EndLine"]
            
            #changing the FileContent to the modified values
            self.FileContent = self.FileContent.replace(MatchObject.group(0), ModifiedLine)

        #update the metadata file about the config
        self.ExportToJson()

        #update the actual config file
        try:
            OutputFile = open(self.FilePath, "w")
            OutputFile.write(self.FileContent)
            OutputFile.close()
            try:
                if self.ApplyCommand: #if the user defined a command to run after applying 
                    os.system(ApplyCommand) 
            except: 
                print("wasnt able to run the shell defined in the config profile")
        except:
            print("wasnt able to write to the file")

    #dumps the class properties to an external json file. the json file path is located i the "MetaDataPath" variable
    def ExportToJson(self):
        MetaDataFile = open(self.MetaDataPath, "w")
        MetaDataFile.write(json.dumps(self.__dict__, indent=4))
