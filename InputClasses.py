import re, os
from PySide6.QtWidgets import QWidget, QCheckBox, QHBoxLayout, QLayout, QLineEdit, QSlider, QLabel, QComboBox, QFontComboBox, QPushButton, QFileDialog, QColorDialog
from PySide6.QtCore import Qt, Signal
from PySide6.QtGui import QColor
from PySide6 import QtCore

#base class for all input widgets
class BaseInput(QWidget):
    Changed = Signal(bool)

    def __init__(self, Value, InputData):
        super(BaseInput, self).__init__()

        self.Result = Value 
        self.InitialValue = Value

        self.Layout = QHBoxLayout(self)
        QLayout.setAlignment(self.Layout, Qt.Alignment.AlignRight) #sets the alignment of the elements in the VLayout to stack from right

    def HasUnsavedChanges(self):
        return self.Result != self.InitialValue

#a class used to input bool values
class BoolInput(BaseInput):
    #Value: the initial value of the input
    #InputData: miscellaneous data about the input widget
    #
    #InputData elements:
    #"Capitalize" - whether the result bool value is upper case or lower case when turned into a string
    def __init__(self, Value, InputData):

        Value = True if Value == "True" or Value == "true" else False

        super(BoolInput, self).__init__(Value, InputData)
        
        #turning the Value into bool from string

        self.Capitalize = InputData["Capitalize"]

        #adding the check box
        self.CheckBox = QCheckBox()
        self.Layout.addWidget(self.CheckBox)

        #setting the check box so it has the same value as in file
        if Value == True:
            self.CheckBox.setCheckState(Qt.Checked)
        else:
            self.CheckBox.setCheckState(Qt.Unchecked)

        self.CheckBox.stateChanged.connect(self.RegisterChange) 


    def RegisterChange(self, state):
        self.Result = state == 2 #the state normally is 0 for unchecked and 2 for checked, i find that weird so i turned it into a normal bool value
        self.Changed.emit(self.HasUnsavedChanges())

    def ReturnToInitialValue(self):
        if self.InitialValue:
            self.CheckBox.setCheckState(Qt.Checked)
        else:
            self.CheckBox.setCheckState(Qt.Unchecked)

    def GetResults(self):
        if self.Capitalize:
            return str(self.Result).capitalize()
        else:
            return str(self.Result).lower()

#a class used to input intager values
class IntInput(BaseInput):
    #Value: the initial value of the input
    #InputData: miscellaneous data about the input widget
    #
    #InputData elements:
    #"MinValue" - the minimum value
    #"MaxValue" - the maximum value
    #"Slider" - whether the input method is a slider or a prompt
    def __init__(self, Value, InputData):

        Value = int(Value) #turning Value from string to int

        super(IntInput, self).__init__(Value, InputData)
        
        self.MinValue = InputData["MinValue"]
        self.MaxValue = InputData["MaxValue"]
        self.Slider = InputData["Slider"]

        #adding the input widget, can be a slider or a QLineEdit
        if self.Slider:
            self.Slider = QSlider()
            self.Slider.setMinimum(self.MinValue) if self.MinValue else self.Slider.setMinimum(0)
            self.Slider.setMaximum(self.MaxValue) if self.MaxValue else self.Slider.setMaximum(100)
            self.Slider.setValue(Value)
            self.Slider.setOrientation(Qt.Orientation.Horizontal)
            self.Slider.setTickPosition(QSlider.TicksAbove)

            self.SliderLabel = QLabel(str(Value))
            self.SliderLabel.setFixedSize(60, 100)


            self.Slider.valueChanged.connect(self.RegisterChange)

            self.Layout.addWidget(self.Slider)
            self.Layout.addWidget(self.SliderLabel)
        else:
            self.Prompt = QLineEdit()
            self.Prompt.setText(str(self.Result))
            self.Layout.addWidget(self.Prompt)
            self.Prompt.editingFinished.connect(self.RegisterChange)


    def RegisterChange(self):
        if self.Slider:
            self.Result = self.Slider.value()
            self.SliderLabel.setText(str(self.Result))
            self.Changed.emit(self.HasUnsavedChanges())
        else:
            PromptText = self.Prompt.text()
            if re.search("^-?\\d*$", PromptText):
                PromptText = int(PromptText)

                #in the if statments below i intentionally specify "self.MaxValue != None" instead of just "self.MaxValue" because self.MaxValue might be equal to 0 and then the program thinks there is not max limit
                #same with self.MinValue

                if self.MaxValue != None and PromptText > self.MaxValue:
                    self.Result = self.MaxValue
                elif self.MinValue != None and PromptText < self.MinValue:
                    self.Result = self.MinValue
                else:
                    self.Result = PromptText
                self.Changed.emit(self.HasUnsavedChanges())

            self.Prompt.setText(str(self.Result))


    def ReturnToInitialValue(self):
        if self.Slider:
            self.Slider.setValue(self.InitialValue)
        else:
            self.Prompt.setText(str(self.InitialValue))

    def GetResults(self):
        return str(self.Result)

#a class used to input float values
class FloatInput(BaseInput):
    #Value: the initial value of the input
    #InputData: miscellaneous data about the input widget
    #
    #InputData elements:
    #"MinValue" - the minimum value
    #"MaxValue" - the maximum value
    #"Precision" - the precision of the float value, aka the amount of digits after the comma
    #"Slider" - whether the input method is a slider or a prompt
    def __init__(self, Value, InputData):

        Value = float(Value) #turning Value from string to float

        super(FloatInput, self).__init__(Value, InputData)
        
        self.MinValue = InputData["MinValue"]
        self.MaxValue = InputData["MaxValue"]
        self.Precision = InputData["Precision"]
        self.Slider = InputData["Slider"]

        #adding the input widget, can be a slider or a QLineEdit

        #important: since the QSlider only supports intager values i used a hack to make it work with float values
        #since the player inputs the precision of the float i know that the "float * 10 ** precision" always returns a intager value
        #however after i divide the returned value by "10 ** precision" it gives the original float value
        if self.Slider:
            self.Slider = QSlider()
            self.Slider.setMinimum(self.MinValue * 10 ** self.Precision)
            self.Slider.setMaximum(self.MaxValue * 10 ** self.Precision) 
            self.Slider.setValue(Value * 10 ** self.Precision)
            self.Slider.setOrientation(Qt.Orientation.Horizontal)
            self.Slider.setTickPosition(QSlider.TicksAbove)
            self.Slider.setTickInterval((self.MaxValue + self.MinValue) * 10 ** self.Precision) #funky math to ensure that the tick interval is always bigger than the whole slider

            self.SliderLabel = QLabel(str(Value))
            self.SliderLabel.setFixedSize(60, 100)

            self.Slider.valueChanged.connect(self.RegisterChange)

            self.Layout.addWidget(self.Slider)
            self.Layout.addWidget(self.SliderLabel)
        else:
            self.Prompt = QLineEdit()
            self.Prompt.setText(str(self.Result))
            self.Layout.addWidget(self.Prompt)
            self.Prompt.editingFinished.connect(self.RegisterChange)



    def RegisterChange(self):
        if self.Slider:
            self.Result = self.Slider.value() / (10 ** self.Precision)
            self.SliderLabel.setText(str(self.Result))
        else:
            PromptText = self.Prompt.text()
            if re.search("\d+([.,]\d+)?", PromptText):
                PromptText = float(PromptText)

                #in the if statments below i intentionally specify "self.MaxValue != None" instead of just "self.MaxValue" because self.MaxValue might be equal to 0 and then the program thinks there is not max limit
                #same with self.MinValue
                if self.MaxValue != None and PromptText > self.MaxValue:
                    self.Result = self.MaxValue
                elif self.MinValue != None and PromptText < self.MinValue:
                    self.Result = self.MinValue
                else:
                    self.Result = PromptText
                self.Result = round(self.Result, self.Precision)
            
            self.Prompt.setText(str(self.Result))
        self.Changed.emit(self.HasUnsavedChanges())

    def ReturnToInitialValue(self):
        if self.Slider:
            self.Slider.setValue(self.InitialValue * 10 ** self.Precision)
        else:
            self.Prompt.setText(str(self.InitialValue))

    def GetResults(self):
        return str(self.Result)

#a class used to input string values
class StringInput(BaseInput):
    #Value: the initial value of the input
    #InputData: miscellaneous data about the input widget
    def __init__(self, Value, InputData):
        super(StringInput, self).__init__(Value, InputData)

        self.Prompt = QLineEdit()
        self.Prompt.setText(self.Result)
        self.Layout.addWidget(self.Prompt)
        self.Prompt.editingFinished.connect(self.RegisterChange)



    def RegisterChange(self):
        self.Result = self.Prompt.text()
        self.Changed.emit(self.HasUnsavedChanges())

    def ReturnToInitialValue(self):
        self.Prompt.setText(self.InitialValue)

    def GetResults(self):
        return self.Result

    def HasUnsavedChanges(self):
        return self.Result != self.InitialValue

#a class used to input enum values, aka a list of possible options
class ListInput(BaseInput):
    #Value: the initial value of the input
    #InputData: miscellaneous data about the input widget
    #
    #InputData elements:
    #"Values" - a list containing the possible values
    def __init__(self, Value, InputData):
        super(ListInput, self).__init__(Value, InputData)

        self.ComboBox = QComboBox()
        self.ComboBox.setFixedSize(250,25)
        self.ComboBox.setEditable(False)
        self.ComboBox.addItems(InputData["Values"])
        self.ComboBox.setCurrentText(self.InitialValue)

        self.ComboBox.currentTextChanged.connect(self.RegisterChange)
        self.Layout.addWidget(self.ComboBox)

    def RegisterChange(self, NewValue):
        self.Result = NewValue
        self.Changed.emit(self.HasUnsavedChanges())

    def ReturnToInitialValue(self):
        self.ComboBox.setCurrentText(self.InitialValue)

    def GetResults(self):
        return self.Result


#a class used to input fonts
class FontInput(BaseInput):
    #Value: the initial value of the input
    #InputData: miscellaneous data about the input widget
    def __init__(self, Value, InputData):
        super(FontInput, self).__init__(Value, InputData)

        self.FontComboBox = QFontComboBox()
        self.FontComboBox.setFixedSize(250,25)
        self.FontComboBox.setEditable(False)
        self.FontComboBox.setCurrentFont(self.InitialValue)

        self.FontComboBox.currentFontChanged.connect(self.RegisterChange)
        self.Layout.addWidget(self.FontComboBox)



    def RegisterChange(self, NewValue):
        self.Result = NewValue.toString().split(",")[0]
        self.Changed.emit(self.HasUnsavedChanges())

    def ReturnToInitialValue(self):
        self.FontComboBox.setCurrentFont(self.InitialValue)

    def GetResults(self):
        return self.Result

#a class used to input file paths
class FilePathInput(BaseInput):
    #Value: the initial value of the input
    #InputData: miscellaneous data about the input widget
    def __init__(self, Value, InputData):
        super(FilePathInput, self).__init__(Value, InputData)
        
        self.FilePathPrompt = QLineEdit(Value)
        self.FilePathPrompt.setFixedSize(250,25)
        self.DialogButton = QPushButton("search")
        self.DialogButton.setFixedSize(75,25)

        self.DialogButton.clicked.connect(self.ChooseFile)
        self.FilePathPrompt.editingFinished.connect(self.RegisterChange)

        self.Layout.addWidget(self.FilePathPrompt)
        self.Layout.addWidget(self.DialogButton)

    def ChooseFile(self):
        FileName = QFileDialog.getOpenFileName(self, "Chose file", "/home/{}/".format(os.getlogin()), )[0]
        self.FilePathPrompt.setText(FileName)
        self.RegisterChange()

    def RegisterChange(self):
        self.Result = self.FilePathPrompt.text()
        self.Changed.emit(self.HasUnsavedChanges())

    def ReturnToInitialValue(self):
        self.FilePathPrompt.setText(self.InitialValue)

    def GetResults(self):
        return self.Result

#a class used to input colors
class ColorInput(BaseInput):
    #Value: the initial value of the input
    #InputData: miscellaneous data about the input widget
    #
    #InputData elements:
    #"Format" - the format the color should be outptted to
    def __init__(self, Value, InputData):
        
        Value = QColor.fromString(Value) #changing from string to QColor

        super(ColorInput, self).__init__(Value, InputData)

        self.Format = InputData["Format"]

        self.ColorPickerButton = QPushButton()
        self.ColorPickerButton.setFixedWidth(150)

        self.ColorPicker = QColorDialog()

        self.ColorPickerButton.clicked.connect(self.PickColor)
        self.ColorPickerButton.setFlat(True)
        self.ColorPickerButton.setAutoFillBackground(True)
        self.ColorPickerButton.setAttribute(QtCore.Qt.WA_StyledBackground, True)
        self.ColorPickerButton.setStyleSheet('background-color: {}'.format(self.Result.name(QColor.HexRgb)))

        self.Layout.addWidget(self.ColorPickerButton)

    def PickColor(self):
        SelectedColor = self.ColorPicker.getColor(self.Result)
        if SelectedColor.isValid():
            self.RegisterChange(SelectedColor)

    def RegisterChange(self, NewValue):
        self.Result = NewValue
        self.Changed.emit(self.HasUnsavedChanges())
        self.ColorPickerButton.setStyleSheet('background-color: {}'.format(self.Result.name(QColor.HexRgb)))

    def ReturnToInitialValue(self):
        self.RegisterChange(self.InitialValue)

    def GetResults(self):
        match self.Format:
            case "HEX":
                return self.Result.name(QColor.HexRgb)

DataTypesInputs = {
        "Bool" : BoolInput,
        "Int" : IntInput,
        "Float" : FloatInput,
        "String" : StringInput,
        "List" : ListInput,
        "Font" : FontInput,
        "FilePath" : FilePathInput,
        "Color" : ColorInput
        }
