import re
from PySide6.QtWidgets import QWidget, QCheckBox, QHBoxLayout, QLayout, QLineEdit, QLabel, QSizePolicy, QComboBox, QPushButton, QVBoxLayout, QGridLayout
from PySide6.QtCore import Qt

class BaseClass(QWidget):
    def __init__(self, InputData):
        super(BaseClass, self).__init__()
        self.Layout = QHBoxLayout(self)
        QLayout.setAlignment(self.Layout, Qt.Alignment.AlignRight) #sets the alignment of the elements in the Layout to stack from right
        self.setFixedHeight(40)

class BoolInput(BaseClass):
    def __init__(self, InputData):
        super(BoolInput, self).__init__(InputData)
        self.Capitalize = InputData["Capitalize"]

        self.CheckBox = QCheckBox("Capitalize Bool Values?")
        if self.Capitalize:
            self.CheckBox.setCheckState(Qt.Checked)
        else:
            self.CheckBox.setCheckState(Qt.Unchecked)
        self.CheckBox.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        self.CheckBox.stateChanged.connect(self.RegisterChange)

        self.Layout.addWidget(self.CheckBox)

    def GetDataTypeMetadata(self):
        return {"Type" : "Bool", "Capitalize" : self.Capitalize}
    
    def RegisterChange(self, state):
        self.Capitalize = state == 2 #the state normally is 0 for unchecked and 2 for checked, i find that weird so i turned it into a normal bool value

class IntInput(QWidget):
    def __init__(self, InputData):
        super(IntInput, self).__init__()

        self.Layout = QGridLayout()
        self.setLayout(self.Layout)

        self.MaxValue = InputData["MaxValue"]
        self.MinValue = InputData["MinValue"]
        self.Slider = InputData["Slider"]

        self.SliderCheckBox = QCheckBox("Slider")
        if self.Slider:
            self.SliderCheckBox.setCheckState(Qt.Checked)
        else:
            self.SliderCheckBox.setCheckState(Qt.Unchecked)
        self.SliderCheckBox.stateChanged.connect(self.SliderCheckBoxChanged)
        self.SliderCheckBox.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)

        self.MinValueLabel = QLabel("Minimum:")
        self.MinValueLabel.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)

        self.MinValuePrompt = QLineEdit(str(self.MinValue))
        self.MinValuePrompt.editingFinished.connect(self.MinValueChanged)
        self.MinValuePrompt.setFixedWidth(50)

        self.MaxValueLabel = QLabel("Maximum:")
        self.MaxValueLabel.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)

        self.MaxValuePrompt = QLineEdit(str(self.MaxValue))
        self.MaxValuePrompt.editingFinished.connect(self.MaxValueChanged)
        self.MaxValuePrompt.setFixedWidth(50)
        
        self.Layout.addWidget(self.SliderCheckBox, 1, 3, alignment=Qt.Alignment.AlignRight)
        self.Layout.addWidget(self.MinValueLabel, 0, 0, alignment=Qt.Alignment.AlignRight)
        self.Layout.addWidget(self.MinValuePrompt, 0, 1)
        self.Layout.addWidget(self.MaxValueLabel, 0, 2, alignment=Qt.Alignment.AlignRight)
        self.Layout.addWidget(self.MaxValuePrompt, 0, 3)

    def GetDataTypeMetadata(self):
        return {"Type" : "Int", "MinValue" : self.MinValue, "MaxValue" : self.MaxValue, "Slider" : self.Slider}

    def SliderCheckBoxChanged(self, state):
        self.Slider = state == 2 #the state normally is 0 for unchecked and 2 for checked, i find that weird so i turned it into a normal bool value

        if self.Slider:
            #if eiher the max value or the min value isnt a number then asign a default value to them
            #this is done because sliders need a limited range so there must be a limit on both max value and the min value
            PromptText = self.MinValuePrompt.text().strip()
            if not re.search("^-?\\d+$", PromptText):
                self.MinValue = int(0)
                self.MinValuePrompt.setText(str(self.MinValue))
            PromptText = self.MaxValuePrompt.text().strip()
            if not re.search("^-?\\d+$", PromptText):
                self.MaxValue = int(10)
                self.MaxValuePrompt.setText(str(self.MaxValue))

    def MinValueChanged(self):
        PromptText = self.MinValuePrompt.text().strip()
        #if the inputted text is even a intager
        if re.search("^-?\\d+$", PromptText):
            self.MinValue = int(PromptText)
        elif PromptText in ["None", "none", ""] and not self.Slider:
            self.MinValue = None
        self.MinValuePrompt.setText(str(self.MinValue))

    def MaxValueChanged(self):
        PromptText = self.MaxValuePrompt.text().strip()
        #if the inputted text is even a intager
        if re.search("^-?\\d+$", PromptText):
            self.MaxValue = int(PromptText)
        elif PromptText in ["None", "none", ""] and not self.Slider:
            self.MaxValue = None
        self.MaxValuePrompt.setText(str(self.MaxValue))

class FloatInput(QWidget):
    def __init__(self, InputData):
        super(FloatInput, self).__init__()

        self.Layout = QGridLayout()
        self.setLayout(self.Layout)

        self.MaxValue = InputData["MaxValue"]
        self.MinValue = InputData["MinValue"]
        self.Slider = InputData["Slider"]
        self.Precision = InputData["Precision"]

        self.SliderCheckBox = QCheckBox("Slider")
        if self.Slider:
            self.SliderCheckBox.setCheckState(Qt.Checked)
        else:
            self.SliderCheckBox.setCheckState(Qt.Unchecked)
        self.SliderCheckBox.stateChanged.connect(self.SliderCheckBoxChanged)
        #self.SliderCheckBox.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)

        self.MinValueLabel = QLabel("Minimum:")
        self.MinValueLabel.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)

        self.MinValuePrompt = QLineEdit(str(self.MinValue))
        self.MinValuePrompt.editingFinished.connect(self.MinValueChanged)
        self.MinValuePrompt.setFixedWidth(50)

        self.MaxValueLabel = QLabel("Maximum:")
        self.MaxValueLabel.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)

        self.MaxValuePrompt = QLineEdit(str(self.MaxValue))
        self.MaxValuePrompt.editingFinished.connect(self.MaxValueChanged)
        self.MaxValuePrompt.setFixedWidth(50)

        self.PrecisionLabel = QLabel("Precision")
        self.PrecisionLabel.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)

        self.PrecisionPrompt = QLineEdit(str(self.Precision))
        self.PrecisionPrompt.editingFinished.connect(self.PrecisionChanged)
        self.PrecisionPrompt.setFixedWidth(50)
        
        self.Layout.addWidget(self.MinValueLabel,    0,  0,  alignment=Qt.Alignment.AlignRight  )                                     
        self.Layout.addWidget(self.MinValuePrompt,   0,  1)                                     
        self.Layout.addWidget(self.MaxValueLabel,    0,  2,  alignment=Qt.Alignment.AlignRight  )                                     
        self.Layout.addWidget(self.MaxValuePrompt,   0,  3)                                     
        self.Layout.addWidget(self.PrecisionLabel,   1,  0,  alignment=Qt.Alignment.AlignRight  )                                     
        self.Layout.addWidget(self.PrecisionPrompt,  1,  1)                                     
        self.Layout.addWidget(self.SliderCheckBox,   1,  3,  alignment=Qt.Alignment.AlignRight  )


    def GetDataTypeMetadata(self):
        return {"Type" : "Float", "MinValue" : self.MinValue, "MaxValue" : self.MaxValue, "Slider" : self.Slider, "Precision" : self.Precision}

    def SliderCheckBoxChanged(self, state):
        self.Slider = state == 2 #the state normally is 0 for unchecked and 2 for checked, i find that weird so i turned it into a normal bool value

        if self.Slider:
            #if eiher the max value or the min value isnt a number then asign a default value to them
            #this is done because sliders need a limited range so there must be a limit on both max value and the min value
            PromptText = self.MinValuePrompt.text().strip()
            if not re.search("^-?\\d+(\.\\d+)?$", PromptText):
                #adding a special condition to ensure that the MaxValue is always bigger than MinValue
                if 0 < self.MaxValue:
                    self.MinValue = float(0)
                else:
                    self.MinValue = self.MaxValue - 1
                self.MinValuePrompt.setText(str(self.MinValue))
            PromptText = self.MaxValuePrompt.text().strip()
            if not re.search("^-?\\d+(\.\\d+)?$", PromptText):
                #adding a special condition to ensure that the Max value is always bigger than MinValue
                if 1 > self.MinValue:
                    self.MaxValue = float(1)
                else:  
                    self.MaxValue = self.MinValue + 1
                self.MaxValuePrompt.setText(str(self.MaxValue))
            PromptText = self.PrecisionPrompt.text().strip()
            if self.Precision > 6: #if turning on the slider it limits the precision to 6 since its very laggy with higher precision
                self.Precision = 6
                self.PrecisionPrompt.setText(str(self.Precision))

    def MinValueChanged(self):
        PromptText = self.MinValuePrompt.text().strip()
        #if the inputted text is even a intager
        if re.search("^-?\\d+(\.\\d+)?$", PromptText):
            if float(PromptText) > self.MaxValue: #in case the user sets the Min value to be more than max Value
                self.MinValue = self.MaxValue - 1
            else:
                self.MinValue = float(PromptText)
        elif PromptText in ["None", "none", ""] and not self.Slider:
            self.MinValue = None
        self.MinValuePrompt.setText(str(self.MinValue))

    def MaxValueChanged(self):
        PromptText = self.MaxValuePrompt.text().strip()
        #if the inputted text is even a intager
        if re.search("^-?\\d+(\.\\d+)?$", PromptText):
            if float(PromptText) < self.MinValue: #in case the user sets the Max value to be less than Min Value
                self.MaxValue = self.MinValue + 1
            else:
                self.MaxValue = float(PromptText)
        elif PromptText in ["None", "none", ""] and not self.Slider:
            self.MaxValue = None
        self.MaxValuePrompt.setText(str(self.MaxValue))

    def PrecisionChanged(self):
        PromptText = self.PrecisionPrompt.text().strip()
        #if the inputted text is even a intager
        if re.search("^\\d+$", PromptText):
            if int(PromptText) >= 6 and self.Slider: #if using the slider then limit the precision to 6 because otherwise its very laggy
                self.Precision = 6
            else:
                self.Precision = int(PromptText)
        self.PrecisionPrompt.setText(str(self.Precision))

class BlankClass(BaseClass):
    def __init__(self, InputData):
        super(BlankClass, self).__init__(InputData)
        self.DataType = InputData

    def GetDataTypeMetadata(self):
        return self.DataType #simply copies what was initially passed to it

class ListInput(BaseClass):
    def __init__(self, InputData):
        super(ListInput, self).__init__(InputData)
        self.Values = InputData["Values"]

        self.RemoveButton = QPushButton("x")
        self.RemoveButton.clicked.connect(self.RemoveCurrentItem)
        self.RemoveButton.setFixedWidth(25)

        self.ComboBox = QComboBox()
        self.ComboBox.addItems(self.Values)
        self.ComboBox.setEditable(True)
        self.ComboBox.setFixedWidth(200)

        self.Layout.addWidget(self.RemoveButton)
        self.Layout.addWidget(self.ComboBox)

    def GetDataTypeMetadata(self):
        ComboBoxValues = []
        for i in range(self.ComboBox.count()):
            ComboBoxValues.append(self.ComboBox.itemText(i))
        self.Values = ComboBoxValues

        return {"Type" : "List", "Values" : self.Values}

    def RemoveCurrentItem(self):
        self.ComboBox.removeItem(self.ComboBox.currentIndex())



DataTypeWidgets = {
        "Bool" : BoolInput,
        "Int" : IntInput,
        "Float" : FloatInput,
        "String" : BlankClass,
        "List" : ListInput,
        "Font" : BlankClass,
        "FilePath" : BlankClass,
        "Color" : BlankClass
        }
        
